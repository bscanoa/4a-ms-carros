from django.urls import path, include

from carros import views

urlpatterns = [
    path('three-cars/', views.ThreeCarsList.as_view()),
    path('all-cars/', views.AllCarsListNoHome.as_view()),
    path('cars/search/', views.search),

    path('cars/<slug:category_slug>/<slug:car_slug>/', views.CarDetail.as_view()),
    path('cars/<slug:category_slug>/', views.CategoryDetail.as_view()),
    path('carsm/<slug:marca_slug>/', views.MarcaDetail.as_view()),

    # marca-urls
    path('marca/<slug:marca_slug>/', views.marca_operations),
    path('marca/create/', views.marca_create),

   # state url
    path('state/<slug:car_slug>', views.state_reservation),

    # reserva
    path('reserva/', views.view_all_reservations),
    path('reserva/book/', views.book_car), 
    path('reserva/<int:rent_pk>/', views.view_reservation_details),
    path('reserva/<int:rent_pk>/extend/', views.extend_reservation_date), 
    path('reserva/<int:rent_pk>/cancel/', views.cancel_reservation),
]