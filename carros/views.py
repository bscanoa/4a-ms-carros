import json
from PIL.Image import SAVE_ALL
from django.db.models import Q
from django.http import Http404
from datetime import date
import datetime
import pytz

utc=pytz.UTC

from django.http.response import JsonResponse
from rest_framework import serializers
from rest_framework.parsers import JSONParser 
from rest_framework import status
from rest_framework.serializers import Serializer

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view

from django.conf import settings
from rest_framework import status
from rest_framework.response import Response
from rest_framework_simplejwt.views import TokenVerifyView
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework_simplejwt.exceptions import InvalidToken, TokenError
from rest_framework_simplejwt.serializers import TokenVerifySerializer

from .models import Marca, Carro, Categoria, Reservacion
from .serializer import CarroSerializer, MarcaSerializer, CategoriaSerializer, MarcaCSerializer, StateReservationSerializer, ReservationSerializer, CarReservationSerializer

# Create your views here.
class ThreeCarsList(APIView):
    def get(self, request, format=None):
        #products = Productos.objects.all().order_by('?')[0:3]
        cars = Carro.objects.all()[0:3]
        serializer = CarroSerializer(cars, many=True)
        return Response(serializer.data)

class AllCarsListNoHome(APIView):
    def get(self, request, format=None):
        cars = Carro.objects.all()
        serializer = CarroSerializer(cars, many=True)
        return Response(serializer.data)

class CarDetail(APIView):
    def get_object(self, category_slug, car_slug):
        try:
            return Carro.objects.filter(categoria__slug=category_slug).get(slug=car_slug)
        except Carro.DoesNotExist:
            raise Http404
    
    def get(self, request, category_slug, car_slug, format=None):
        car = self.get_object(category_slug, car_slug)
        serializer = CarroSerializer(car)
        return Response(serializer.data)

class CategoryDetail(APIView):
    def get_object(self, category_slug):
        try:
            return Categoria.objects.get(slug=category_slug)
        except Categoria.DoesNotExist:
            raise Http404
    
    def get(self, request, category_slug, format=None):
        category = self.get_object(category_slug)
        serializer = CategoriaSerializer(category)
        return Response(serializer.data)

class CategoryDetail(APIView):
    def get_object(self, category_slug):
        try:
            return Categoria.objects.get(slug=category_slug)
        except Categoria.DoesNotExist:
            raise Http404
    
    def get(self, request, category_slug, format=None):
        category = self.get_object(category_slug)
        serializer = CategoriaSerializer(category)
        return Response(serializer.data)    

# Crear vista de Marca  
class MarcaDetail(APIView):
    def get_object(self, marca_slug):
        try:
            return Marca.objects.get(slug=marca_slug)
        except Marca.DoesNotExist:
            raise Http404
    
    def get(self, request, marca_slug, format=None):
        marca = self.get_object(marca_slug)
        serializer = MarcaSerializer(marca)
        return Response(serializer.data)

@api_view(['POST'])
def search(request):
    query = request.data.get('query', '')

    if query:
        cars = Carro.objects.filter(Q(nombre__icontains=query) | Q(descripcion__icontains=query))
        serializer = CarroSerializer(cars, many=True)
        return Response(serializer.data)
    else:
        return Response({"products": []})

# Crear funciones para CRUD de Carro

# CRUD Marcas
@api_view(['POST'])
def marca_create(request):
    m_serializer = MarcaCSerializer(data=request.data)
    if m_serializer.is_valid():
        m_serializer.save()
    return JsonResponse(m_serializer.data)

@api_view(['GET', 'PUT', 'DELETE'])
def marca_operations(request, marca_slug):
    try: 
        marca = Marca.objects.get(slug=marca_slug) 
    except Marca.DoesNotExist: 
        return JsonResponse({'message': 'La marca no existe'}, status=status.HTTP_404_NOT_FOUND) 
 
    if request.method == 'GET': 
        marca = MarcaCSerializer(marca) 
        return JsonResponse(marca.data) 
    # update
    elif request.method == 'PUT': 
        m_data = JSONParser().parse(request) 
        m_serializer = MarcaCSerializer(marca, data=m_data) 
        if m_serializer.is_valid(): 
            m_serializer.save() 
            return JsonResponse(m_serializer.data) 
        return JsonResponse(m_serializer.errors, status=status.HTTP_400_BAD_REQUEST) 
    # delete
    elif request.method == 'DELETE': 
        marca.delete() 
        return JsonResponse({'message': 'La marca ha sido borrado exitosamente!'}, status=status.HTTP_204_NO_CONTENT)

@api_view(['PUT'])
def state_reservation(request, car_slug):
    try:
        car = Carro.objects.get(slug=car_slug)
    except:
        return JsonResponse({'message': 'El carro no existe'}, status=status.HTTP_404_NOT_FOUND) 
    
    state = JSONParser().parse(request)
    serializer = StateReservationSerializer(car, data=state)
    if serializer.is_valid():
        serializer.save()
        return JsonResponse(serializer.data)
    return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# Operaciones para reservación

@api_view(['GET'])
def view_all_reservations(request):
    if request.method == 'GET':
        reservations = Reservacion.objects.all()
        serializer = ReservationSerializer(reservations, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def view_reservation_details(request, rent_pk):
    try:
        reservation = Reservacion.objects.get(pk=rent_pk)
    except Reservacion.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ReservationSerializer(reservation)
        return Response(serializer.data)


@api_view(['POST'])
def book_car(request):
    if request.method == 'POST':
        serializer = ReservationSerializer(data=request.data)
            
        if serializer.is_valid():
            current_date = datetime.datetime.today()
            fecha_inicio = serializer.validated_data['fecha_inicio']
            fecha_final = serializer.validated_data['fecha_final']
            
            current_date = current_date.replace(tzinfo=utc)
            fecha_inicio = fecha_inicio.replace(tzinfo=utc)
            fecha_final = fecha_final.replace(tzinfo=utc)
            
            car = serializer.validated_data['car']
            reservations = Reservacion.objects.all().filter(car=car.id)

            # Check if the issue_date of new reservation doesn't clash with any previous reservations
            for r in reservations:
                if r.fecha_inicio <= fecha_inicio <= r.fecha_final:
                    content = {"message":"The selected car is not available on this date"}
                    return Response(data=json.dumps(content), status=status.HTTP_400_BAD_REQUEST)

            # Check whether issue_date is not older than today's date, and is less equal to return_date
            if current_date <= fecha_inicio and fecha_inicio <= fecha_final:
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['PUT'])
def extend_reservation_date(request, rent_pk):
    try:
        reservation = Reservacion.objects.get(pk=rent_pk)
    except Reservacion.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'PUT':
        serializer = ReservationSerializer(reservation, data=request.data)

        if serializer.is_valid():
            current_date = date.today()
            issue_date = serializer.validated_data['issue_date']
            return_date = serializer.validated_data['return_date']

            car = serializer.validated_data['car']
            reservations = Reservacion.objects.all().filter(car=car.id)

            # Check if the return_date of new reservation doesn't clash with any previous reservations
            for r in reservations:
                if r.issue_date <= return_date <= r.return_date:
                    res = {"message":"Failed to extend the date. Car is not available."}
                    return Response(data=json.dumps(res), status=status.HTTP_400_BAD_REQUEST)

            # Check whether issue_date is not older than today's date, and is less equal to return_date
            if current_date <= issue_date and issue_date <= return_date:
                serializer.save()
                return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['DELETE'])
def cancel_reservation(request, rent_pk):
    try:
        reservation = Reservacion.objects.get(pk=rent_pk)
    except Reservacion.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'DELETE':
        reservation.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)