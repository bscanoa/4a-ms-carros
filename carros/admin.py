from django.contrib import admin

# Register your models here.
from .models import Categoria, Carro, Marca, Reservacion

admin.site.register(Categoria)
admin.site.register(Carro)
admin.site.register(Marca)
admin.site.register(Reservacion)