from django.db.models import fields
from rest_framework import serializers

from .models import Marca, Carro, Categoria, Reservacion

class CarroSerializer(serializers.ModelSerializer):
    class Meta:
        model = Carro
        fields = (
            "id",
            "nombre",
            "get_absolute_url",
            "descripcion",
            "marca",
            "categoria",
            "disponibilidad",   
            "precio",
            "get_image",
            "get_thumbnail"
        )


class CategoriaSerializer(serializers.ModelSerializer):
    cars = CarroSerializer(many=True)

    class Meta:
        model = Categoria
        fields = (
            "id",
            "name",
            "get_absolute_url",
            "cars",
        )

class MarcaCSerializer(serializers.ModelSerializer):
    class Meta:
        model = Marca
        fields = '__all__'


class MarcaSerializer(serializers.ModelSerializer):
    carros = CarroSerializer(many=True)

    class Meta:
        model = Marca
        fields = (
            "id",
            "nombre",
            "get_absolute_url",
            "carros",
        )

class StateReservationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Carro
        fields = (
            'disponibilidad', 
        )

class CarReservationSerializer(serializers.ModelSerializer):
    car = CarroSerializer()
    class Meta:
        model = Reservacion
        fields = ('car', 'fecha_inicio', 'fecha_final')


class ReservationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reservacion
        fields = '__all__'

class CarDetailsReservationSerializer(serializers.Serializer):
    car = CarroSerializer()
    current_active_bookings = ReservationSerializer(many=True)