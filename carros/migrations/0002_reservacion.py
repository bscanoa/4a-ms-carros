# Generated by Django 3.2.9 on 2021-12-05 23:17

import carros.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('carros', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Reservacion',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=45)),
                ('fecha_inicio', models.DateTimeField(validators=[carros.models.Reservacion.validate_dates_range], verbose_name='fecha de inicio de la reservación')),
                ('fecha_final', models.DateTimeField(validators=[carros.models.Reservacion.validate_dates_range], verbose_name='fecah en que termina la reservación')),
                ('car', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='carros.carro')),
            ],
        ),
    ]
