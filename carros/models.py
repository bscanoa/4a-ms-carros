import requests
from django.db import models
from io import BytesIO
from PIL import Image, ImageDraw, ImageFont

from django.core.files import File
from django.db import models

from django.utils import timezone
from django.core.exceptions import ValidationError

def validate_dates_range(fecha):
    if fecha < timezone.now():
        raise ValidationError('%s no es una fecha valida!' % fecha)

# Create your models here.
class Marca(models.Model):
    nombre = models.CharField(max_length=255) 
    slug = models.SlugField() 
    ciudad = models.CharField(max_length=20)
    telefono = models.CharField(max_length=12)

    class Meta:
        ordering = ('nombre',)
    
    def __str__(self):
        return self.nombre
    
    def get_absolute_url(self):
        return f'/{self.slug}/'


class Categoria(models.Model):
    name = models.CharField(max_length=255) 
    slug = models.SlugField() 

    class Meta:
        ordering = ('name',)
    
    def __str__(self):
        return self.name
    
    def get_absolute_url(self):
        return f'/{self.slug}/'


class Carro(models.Model):
    nombre = models.CharField(max_length = 20)
    slug = models.SlugField()
    categoria = models.ForeignKey(Categoria, related_name='cars', on_delete=models.CASCADE)
    color = models.CharField(max_length = 10)
    marca = models.ForeignKey(Marca, related_name='carros', on_delete=models.CASCADE)
    capacidad = models.CharField(max_length = 2)
    disponibilidad = models.BooleanField(default = True)
    descripcion = models.CharField(max_length = 100)
    precio = models.DecimalField(max_digits=10, decimal_places=2)
    imagen = models.ImageField(upload_to='uploads/', blank=True, null=True)
    imagen_reservado = models.ImageField(upload_to='uploads/', blank=True, null=True)
    fecha_add = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-fecha_add',)
        
    def __str__(self):
        return self.nombre
        
    def get_absolute_url(self):
        return f'/{self.categoria.slug}/{self.slug}/'

    def get_image(self):
        if self.imagen:
            return 'https://car-manage.herokuapp.com' + self.imagen.url
        return ''
    
    def get_thumbnail(self):
        if self.imagen_reservado:
            return 'https://car-manage.herokuapp.com' + self.imagen_reservado.url
        else:
            if self.imagen:
                self.imagen_reservado = self.crear_miniatura(self.imagen)
                self.save()

                return 'https://car-manage.herokuapp.com' + self.imagen_reservado.url
            else:
                return ''
    
    def crear_miniatura(self, image):
        img = Image.open(image)
        img = img.convert('L')
        img_1 = ImageDraw.Draw(img)

        req = requests.get('https://car-manage.herokuapp.com/media/Roboto-BlackItalic.ttf')
        font = ImageFont.truetype(BytesIO(req.content), 60)
        text = "Este carro se encuentra\nRESERVADO"
        # Centrar texto vertical y horizontalmente.
        lines = text.splitlines()
        w = font.getsize(max(lines, key=lambda s: len(s)))[0]
        h = font.getsize(text)[1] * len(lines)
        x, y = img.size
        x /= 2
        x -= w / 2
        y /= 2
        y -= h / 2

        shadowColor = 'black'

        for adj in range(3):
            #move right
            img_1.text((x-adj, y), text, font=font, fill=shadowColor, align="center")
            #move left
            img_1.text((x+adj, y), text, font=font, fill=shadowColor, align="center")
            #move up
            img_1.text((x, y+adj), text, font=font, fill=shadowColor, align="center")
            #move down
            img_1.text((x, y-adj), text, font=font, fill=shadowColor, align="center")
            #diagnal left up
            img_1.text((x-adj, y+adj), text, font=font, fill=shadowColor, align="center")
            #diagnal right up
            img_1.text((x+adj, y+adj), text, font=font, fill=shadowColor, align="center")
            #diagnal left down
            img_1.text((x-adj, y-adj), text, font=font, fill=shadowColor, align="center")
            #diagnal right down
            img_1.text((x+adj, y-adj), text, font=font, fill=shadowColor, align="center")
            
        img_1.multiline_text((x, y), text, font=font, fill="white",
                            align="center")

        thumb_io = BytesIO()
        img.save(thumb_io, 'PNG', quality=85)

        thumbnail = File(thumb_io, name=image.name)

        return thumbnail



class Reservacion(models.Model):
    def validate_dates_range(fecha):
        if fecha < timezone.now():
            raise ValidationError('%s no es una fecha valida!' % fecha)

    car = models.ForeignKey(Carro, related_name='car', on_delete=models.CASCADE)
    fecha_inicio = models.DateTimeField('fecha de inicio de la reservación', validators=[validate_dates_range])
    fecha_final = models.DateTimeField('fecah en que termina la reservación', validators=[validate_dates_range])
    usuario = models.CharField(max_length=20, name='nombre_de_ususario')

    def __str__(self):
        return "Reservación del carro %s desde %s hasta %s" % (self.car.nombre, self.fecha_inicio, self.fecha_final)